module gitlab.com/qnib-golang/go-github

go 1.19

require (
	github.com/google/go-github v11.0.0+incompatible
	github.com/qnib/go-github v0.0.0-20170908103238-be20d2dda189
	github.com/spf13/cobra v0.0.0-20170905172051-b78744579491
	github.com/spf13/viper v1.0.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fsnotify/fsnotify v1.4.2 // indirect
	github.com/google/go-querystring v0.0.0-20170111101155-53e6ce116135 // indirect
	github.com/hashicorp/hcl v0.0.0-20170825171336-8f6b1344a92f // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/magiconair/properties v1.7.3 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20170523030023-d0303fe80992 // indirect
	github.com/pelletier/go-buffruneio v0.2.0 // indirect
	github.com/pelletier/go-toml v1.0.0 // indirect
	github.com/spf13/afero v0.0.0-20170901052352-ee1bd8ee15a1 // indirect
	github.com/spf13/cast v1.1.0 // indirect
	github.com/spf13/jwalterweatherman v0.0.0-20170901151539-12bd96e66386 // indirect
	github.com/spf13/pflag v1.0.0 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	golang.org/x/sys v0.0.0-20170906000021-9aade4d3a3b7 // indirect
	golang.org/x/text v0.0.0-20170901153044-bd91bbf73e9a // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.0.0-20170812160011-eb3733d160e7 // indirect
)
